#ifndef MOJKWADRAT_H
#define MOJKWADRAT_H

//#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QMediaPlayer>

class Mojkwadrat: public QObject,public QGraphicsPixmapItem// public QGraphicsRectItem
{
    Q_OBJECT
public:
    void keyPressEvent(QKeyEvent * zdarzenie);
  public slots:
        void spawn();
private:
        QMediaPlayer * strzal;
};

#endif // MOJKWADRAT_H
