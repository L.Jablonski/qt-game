#include "Pilka.h"
#include "brama.h"
#include "przeciwnik.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <typeinfo>
#include "wynik.h"
#include "gra.h"
#include "mur.h"
extern Gra * gra; //rozszerzamy o inforamcje o globalnych obieckie
Pilka::Pilka()
{
    setPixmap(QPixmap(":/obrazy/SoccerBall.png"));
   //laczymy
    QTimer * timer = new QTimer;
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

            timer->start(50);
}

void Pilka::move(){
    // sprawdzamy kolizje
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; ++i){
        if (typeid(*(colliding_items[i])) == typeid(Przeciwnik)){
            scene()->removeItem(this);
            delete this;
            return;
        }
        if (typeid(*(colliding_items[i])) == typeid(Brama)){
            //zwiekszamy punkty
           gra->wynik->dodaj();
            // usuwamy z pola widoku         
            scene()->removeItem(this);
            // usuwamy z pamieci
             delete this;
            return;
        }
            if (typeid(*(colliding_items[i])) == typeid(Mur)){
                //zwiekszamy punkty
                gra->wynik->dodaj();
                // usuwamy z pola widoku
                scene()->removeItem(this);
                // usuwamy z pamieci
                delete this;
                return;
        }
    }
    //poruszamy pile
    setPos(x(),y()-15);
    if (pos().y() < 0)
    {
        scene()->removeItem(this); // jesli wyjdzie za ekran
        delete this;               // usuwamy z pola widzenia i pamieci
    }
    }

