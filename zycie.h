#ifndef ZYCIE_H
#define ZYCIE_H


#include <QGraphicsTextItem>

class Zycie: public QGraphicsTextItem
{
public:
    Zycie(QGraphicsItem * parent=0);
    void odejmij();
    int wezzycie();
private:
    int zycie;
};



#endif // ZYCIE_H
