#include <QApplication>
#include <QGraphicsScene>
#include "Mojkwadrat.h"
#include <QGraphicsView>
#include <QTimer>
#include "wynik.h"
#include "gra.h"
#include <QGraphicsPixmapItem>
#include "QImage"
#include <QMediaPlayer>
#include "przeciwnik.h"
#include "brama.h"
#include "mur.h"
Gra::Gra(QWidget *parent)
{
    int mur_visible; //zmienna odpoiwadajaca za pojawianie muru

    //tworzenie planszy
    scena = new QGraphicsScene();
    scena->setSceneRect(0,0,489,600);
    setBackgroundBrush(QBrush(QImage(":/obrazy/boisko.png")));
    setScene(scena);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff); //usuwamy paski po bokach
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);   //
    setFixedSize(489,600);

    //tworenie obiektu

    gracz = new Mojkwadrat();
    //mozna na raz poruszać tylko jednym obiektem wiec dajemy fokus na ten obiekt
    gracz->setFlag(QGraphicsItem::ItemIsFocusable);
    gracz->setFocus();
    //dodawanie obiketu
    scena -> addItem(gracz);
    gracz->setPos(245,450);

    //dodajemy bramke
    Brama * brama = new Brama();
    scene()->addItem(brama);

    //dodajemu mur
    Mur * mur =new Mur();
    scene()->addItem(mur);   //dodac licznik obliczajacy pojawianie sie muru w okresach co 30 skeund na 5 sekund
    // scene()->removeItem(mur);

    //dodjemy wynik na ekran
    wynik=new Wynik();
    scena->addItem(wynik);

    //dodajemy zycie
    zycie = new Zycie();
    zycie->setPos(zycie->x(),zycie->y()+25);
    scena->addItem(zycie);

    //tworzenie wrogów
    QTimer * timer = new QTimer();
    QObject::connect(timer,SIGNAL(timeout()),gracz,SLOT(spawn()));
    timer -> start(4000); //bylo 4000

    //gramy muzyczkę

    QMediaPlayer * muzyka= new QMediaPlayer();
    muzyka -> setMedia(QUrl("qrc:/dzwieki/stadium_bcg.mp3"));
    muzyka -> play();

    show();
}

