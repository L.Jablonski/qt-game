#ifndef GRA_H
#define GRA_H


#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include "mojkwadrat.h"
#include "wynik.h"
#include "zycie.h"

class Gra: public QGraphicsView{
public:
    Gra(QWidget * parent=0);

    QGraphicsScene * scena;
    Mojkwadrat * gracz;
    Wynik * wynik;
    Zycie * zycie;

};


#endif // GRA_H

