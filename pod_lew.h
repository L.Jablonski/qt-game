#ifndef POD_LEW_H
#define POD_LEW_H

#include <QGraphicsPixmapItem>
#include <QObject>
class Pod_lew:public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Pod_lew();
public slots:
    void move();

};

#endif // POD_LEW_H
