#include "Mojkwadrat.h"
#include <QKeyEvent>
#include <QGraphicsScene>
#include "Pilka.h"
#include "pod_lew.h"
#include "przeciwnik.h"


void Mojkwadrat::keyPressEvent(QKeyEvent *zdarzenie)
{
    strzal =new QMediaPlayer();
    strzal ->setMedia(QUrl("qrc:/dzwieki/kick.mp3"));
    setPixmap(QPixmap(":/obrazy/gracz.png"));

    if(zdarzenie->key()== Qt::Key_Left)
    {
        if(pos().x() >0)
            setPos(x()-10,y());
    }
    else if(zdarzenie->key()== Qt::Key_Right)
    {
        if(pos().x() + 100 < 486)
            setPos(x()+10,y());
    }
    /*   else if(zdarzenie->key()== Qt::Key_Up)
     {
       // if(pos().y() < 400)
        setPos(x(),y()-10);
    }
    else if(zdarzenie->key()== Qt::Key_Down)
       {
       // if(pos().y()  > 35)
        setPos(x(),y()+10);
    } */

    else if(zdarzenie->key()== Qt::Key_Space)
    {
        //wylatuje piła
        Pilka * pilka = new Pilka();
        pilka->setPos(x(),y());
        scene()->addItem(pilka);

        // odplamy dzwiek piłY
        if(strzal ->state()==QMediaPlayer::PlayingState)
        {
            strzal->setPosition(0);
        }
        else if(strzal->state()==QMediaPlayer::StoppedState)
        {
            strzal->play();
        }
    }
    else if(zdarzenie->key()== Qt::Key_X)
    {
        Pod_lew * pod_lew = new Pod_lew;
        pod_lew->setPos(x(),y());
        scene()->addItem(pod_lew);
        if(strzal ->state()==QMediaPlayer::PlayingState)
        {
            strzal->setPosition(0);
        }
        else if(strzal->state()==QMediaPlayer::StoppedState)
        {
            strzal->play();
        }

    }
}


void Mojkwadrat::spawn()
{
    //  spawnujemy przeciwnika w grze
    Przeciwnik * przeciwnik = new Przeciwnik();
    scene()->addItem(przeciwnik);

}
