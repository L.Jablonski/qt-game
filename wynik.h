#ifndef WYNIK_H
#define WYNIK_H

#include <QGraphicsTextItem>

class Wynik: public QGraphicsTextItem
{
public:
    Wynik(QGraphicsItem * parent=0);
    void dodaj();
    int wezwynik();
private:
    int wynik;
};

#endif // WYNIK_H
