#include "zycie.h"
#include<QFont>

Zycie::Zycie(QGraphicsItem *parent):QGraphicsTextItem(parent)
{
    //inicujemy wynik jako 0
     zycie=100;
     // wypisujemy wynik na ekranie
     setPlainText(QString("Zycie: ")+QString::number(zycie));
     setDefaultTextColor(Qt::red);
     setFont(QFont("times",16));
}
void Zycie::odejmij()
{
    zycie--;
    setPlainText(QString("Zycie: ")+QString::number(zycie));
}
int Zycie::wezzycie()
 {
     return zycie;
 }
