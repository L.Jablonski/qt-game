#include "wynik.h"
#include<QFont>

Wynik::Wynik(QGraphicsItem *parent):QGraphicsTextItem(parent)
{
    //inicujemy wynik jako 0
     wynik=0;
     // wypisujemy wynik na ekranie
     setPlainText(QString("Wynik: ")+QString::number(wynik));
     setDefaultTextColor(Qt::blue);
     setFont(QFont("times",16));
}
void Wynik::dodaj()
{
    wynik++;
    setPlainText(QString("Wynik: ")+QString::number(wynik));
}
int Wynik::wezwynik()
 {
     return wynik;
 }
